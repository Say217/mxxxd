from django import forms

class QueryForm(forms.Form):
    day_attrs = {
        'class' : 'form-control',
        'placeholder' : 'Choose your day',
    }

    time_attrs = {
        'class' : 'form-control',
        'placeholder' : 'HH (24 hours)'
    }

    topic_attrs = {
        'class' : 'form-control'
    }
    
    location_attrs = {
        'class' : 'form-control',
        'placeholder' : 'Input your location (City)',
        'id' : 'loc'
    }

    day_list = [
            ('', 'Choose your day'),
            ('Sun', 'Sunday'),
            ('Mon', 'Monday'),
            ('Tue', 'Tuesday'),
            ('Wed', 'Wednesday'),
            ('Thu', 'Thursday'),
            ('Fri', 'Friday'),
            ('Sat', 'Saturday')
    ]

    topic_list = [
            ('', 'Choose your topic'),
            ('culture', 'Culture'),
            ('food', 'Food'),
            ('music', 'Music'),
            ('education', 'Education'),
            ('history', 'History'),
            ('science', 'Science')
    ]

    ## Input Day 1
    day_1 = forms.ChoiceField(
        label="Day 1", choices=day_list, widget=forms.Select(day_attrs))
    start_time_1 = forms.IntegerField(
        label="Start Time 1",widget=forms.NumberInput(time_attrs))
    end_time_1 = forms.IntegerField(
        label="End Time 1",widget=forms.NumberInput(time_attrs))
    topic_1 = forms.ChoiceField(
        label="Topic 1", choices=topic_list, widget=forms.Select(topic_attrs))

    ## Input Day 2 (optional)
    day_2 = forms.ChoiceField(
        label="Day 2 (Optional)", choices=day_list, required=False, widget=forms.Select(day_attrs))
    start_time_2 = forms.IntegerField(
        label="Start Time 2", required=False, widget=forms.NumberInput(time_attrs))
    end_time_2 = forms.IntegerField(
        label="End Time 2", required=False, widget=forms.NumberInput(time_attrs))
    topic_2 = forms.ChoiceField(
        label="Topic 2 (Optional)", choices=topic_list, required=False, widget=forms.Select(topic_attrs))

    ## Input Day 3 (optional)
    day_3 = forms.ChoiceField(
        label="Day 3 (Optional)", choices=day_list, required=False, widget=forms.Select(day_attrs))
    start_time_3 = forms.IntegerField(
        label="Start Time 3", required=False, widget=forms.NumberInput(time_attrs))
    end_time_3 = forms.IntegerField(
        label="End Time 3", required=False, widget=forms.NumberInput(time_attrs))
    topic_3 = forms.ChoiceField(
        label="Topic 3 (Optional)", choices=topic_list, required=False, widget=forms.Select(topic_attrs))

    location = forms.CharField(label="Location", widget=forms.TextInput(location_attrs))
    