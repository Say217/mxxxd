from django.urls import path
from . import views


urlpatterns=[
	path('', views.index, name='homepage'),
	path('create/', views.createJson, name='create-json'),
	path('generate/', views.generate_another, name='generate-answer')
]
