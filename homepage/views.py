from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect 

from .forms import QueryForm
from eventmatcher.views import process_matches, get_another_answer
import json

result = ""

# Create your views here.
def index(request):
    response = {}
    response['form'] = QueryForm
    return render(request, 'coba.html', response)
    
def createJson(request):
    form = QueryForm(request.POST or None)
    response = {}
    response_data = {}
    if request.method == 'POST':
        location = request.POST.get('location')
        response_data['location'] = location
        response_data['activity'] = []
        for i in range(3):
            day = request.POST.get('day_' + str(i+1))
            start_time = request.POST.get('start_time_' + str(i+1))
            end_time = request.POST.get('end_time_' + str(i+1))
            topic = request.POST.get('topic_' + str(i+1))

            if(start_time == ''):
                response_data['activity'] += [
                {
                    'day' : day,
                    'start_time' : 0,
                    'end_time' : 0,
                    'topic' : topic
                }
                ]

            else:
                response_data['activity'] += [
                    {
                        'day' : day,
                        'start_time' : start_time,
                        'end_time' : end_time,
                        'topic' : topic
                    }
                ]
        global result
        result = process_matches(response_data)
        response["day_1"] = result["activity"][0]["day"]
        response["day_2"] = result["activity"][1]["day"]
        response["day_3"] = result["activity"][2]["day"]
        response["generate"] = True

        for i in range(3):
            if result['activity'][i] != None:
                response['activity_' + str(i+1)] = result['choosen'][i]
            else:
                response['activity_' + str(i+1)] = ""
            print(response['activity_' + str(i+1)])
        return render(request, 'result.html', response)
    
    return HttpResponseRedirect('/')

def generate_another(request):
    global result
    result = get_another_answer(result)
    print(result)
    response = {}
    response["day_1"] = result["activity"][0]["day"]
    response["day_2"] = result["activity"][1]["day"]
    response["day_3"] = result["activity"][2]["day"]
    response["generate"] = True
    for i in range(3):
        if result['activity'][i] != None:
            response['activity_' + str(i+1)] = result['choosen'][i]
        else:
            response['activity_' + str(i+1)] = ""
        print(response['activity_' + str(i+1)])
    
    if response['activity_1'] == None and response['activity_2'] == None and response['activity_3'] == None:
        response["generate"] = False

    return render(request, 'result.html', response)
