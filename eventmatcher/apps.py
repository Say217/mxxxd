from django.apps import AppConfig


class EventmatcherConfig(AppConfig):
    name = 'eventmatcher'
