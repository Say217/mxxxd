# Generated by Django 2.1.1 on 2019-12-11 05:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eventmatcher', '0004_auto_20191206_1634'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='day',
            field=models.CharField(choices=[('Monday', 'Monday'), ('Tuesday', 'Tuesday'), ('Wednesday', 'Wednesday'), ('Thursday', 'Thursday'), ('Friday', 'Friday'), ('Saturday', 'Saturday'), ('Sunday', 'Sunday')], max_length=3),
        ),
    ]
