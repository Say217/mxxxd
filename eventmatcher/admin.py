from django.contrib import admin
from .models import Activity, Schedule

# Register your models here.
admin.site.register(Activity)
admin.site.register(Schedule)