class backtrack():

    def is_safe(self, pref_number, activity):
        """
        A function to check if an activity has been assigned 
        to a schedule
        """
        for pref in range (len(self.preferences['activity'])):
            if pref_number != pref:
                if self.preferences['choosen'][pref] != None:
                    if self.preferences['choosen'][pref].name == activity.name:
                        return False
        return True

    def assignment_util(self, pref_number):
        """
        Core function of this backtracking class
        """
        if pref_number == len(self.preferences['activity']):
            return True

        for activity in self.preferences['possible_activities'][pref_number]:
            if self.is_safe(pref_number, activity) == True:
                self.preferences['choosen'][pref_number] = activity
                self.track += 1
                if self.track > self.longest_track:
                    self.longest_track = self.track
                if self.assignment_util(pref_number+1) == True:
                    return True
                self.track -= 1
                self.preferences['choosen'][pref_number] = None

    def rerun_assignment(self, pref_number):
        """
        Method to attempt returning the best answer
        """
        for activity in self.preferences['possible_activities'][pref_number]:
            if self.is_safe(pref_number, activity) == True:
                self.preferences['choosen'][pref_number] = activity
                self.track += 1
                if self.track == self.longest_track:
                    return True
                if self.rerun_assignment(pref_number+1) == True:
                    return True
                self.track -= 1
                self.preferences['choosen'][pref_number] = None

    def another_run(self, pref_number):
        """
        Method to try getting another answer
        """
        if pref_number == len(self.preferences['activity']):
            return True

        if self.preferences['activity'][pref_number]['day'] == '':
            return True

        for activity in self.preferences['possible_activities'][pref_number]:
            if self.is_safe(pref_number, activity) == True:
                self.preferences['choosen'][pref_number] = activity
                if self.another_run(pref_number+1) == True:
                    if(self.answer_index < self.preferences['answer_index']):
                        self.answer_index += 1
                    else:
                        return True
                self.preferences['choosen'][pref_number] = None

        if not self.preferences['answered'] and pref_number == len(self.preferences['activity']) -1:
            return True

    def activity_assignment(self, preferences):
        """
        The function to start backtracking
        """
        self.track = 0
        self.longest_track = 0
        self.answer_index = 0
        self.preferences = preferences
        self.preferences["answered"] = True
        if self.assignment_util(0) == None:
            self.rerun_assignment(0)
            self.preferences["answered"] = False
        self.preferences['answer_index'] = 1
        return self.preferences

    def another_assignment(self, preferences):
        """
        Main function to get another backtracking answer
        """
        self.preferences = preferences

        self.preferences['choosen'] = [None, None, None]

        self.answer_index = 0
        if self.another_run(0) == None:
            self.preferences["answered"] = False
        self.preferences['answer_index'] += 1
        return self.preferences