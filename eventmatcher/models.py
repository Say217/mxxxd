from django.db import models

# Create your models here.
class Schedule(models.Model):
    DAYS = [
        ('Mon', 'Monday'),
        ('Tue', 'Tuesday'),
        ('Wed', 'Wednesday'),
        ('Thu', 'Thursday'),
        ('Fri', 'Friday'),
        ('Sat', 'Saturday'),
        ('Sun', 'Sunday')
    ]

    day = models.CharField(max_length=3, choices=DAYS)
    open = models.IntegerField()
    close = models.IntegerField()

    def __str__(self):
        return "%s, open: %d close: %d" % (self.day, self.open, self.close)
    
class Activity(models.Model):
    PRIORITY_LEVEL = [
        ('low', 'low'),
        ('med', 'medium'),
        ('hi', 'high')
    ]
    
    name = models.CharField(max_length=50)
    interest = models.CharField(max_length=25)
    location = models.CharField(max_length=50)
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    value = models.IntegerField(default=0)
    priority = models.CharField(max_length=3, choices=PRIORITY_LEVEL, default='low')

    def __str__(self):
        return "%s (%s)" % (self.name, self.schedule.__str__())
