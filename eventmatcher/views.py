import requests
from .models import Activity
from .backtrack import *

# Main processing method
def process_matches(preferences):
    list_of_all_events = Activity.objects.all()
    preferences_interest = []
    preferences_sched = []
    preferences['possible_activities'] = [[],[],[]]
    preferences['answer_index'] = 0
    preferences['choosen'] = [None,None,None]
    x = preferences['activity']
    for i in x:
        preferences_interest.append(i['topic'])
        sched_dict = {
            "day" : i['day'],
            "start" : i['start_time'],
            "end" : i['end_time']
        }
        preferences_sched.append(sched_dict)


    interest_priority(preferences_interest, list_of_all_events)
    distance_priority(preferences['location'], list_of_all_events)

    sorted_by_priority = do_sort(list_of_all_events)
    priority_assignment(sorted_by_priority)

    preferences = scheduling(preferences, preferences_sched, sorted_by_priority)

    bt_obj = backtrack()

    return bt_obj.activity_assignment(preferences)  
   # return parse_to_json(sorted_by_priority)

def get_another_answer(preferences):
    bt_obj = backtrack()
    return bt_obj.another_assignment(preferences)

# Method to assign priority based on interest
def interest_priority(user_interests, data):
    for i in data:
        if i.interest in user_interests:
            i.value = i.value + 10

# Method to assign priority based on schedule
def scheduling(user, user_sched, data):
    for j in range(len(user_sched)):
        for i in data:
            if is_in_schedule(i.schedule, user_sched[j]) or is_intersects_schedule(i.schedule, user_sched[j]):
                user['possible_activities'][j].append(i)
    return user

# Method to assign priority based on distance
def distance_priority(user_location, data):
    origin = user_location
    for i in data:
        maps = get_distance(origin, i.location)
        print (maps)
        maps_rows = maps['rows'][0]

        if len(maps_rows) != 0:
            distance = maps_rows['elements'][0]['distance']['text'][:-1]
            print("AJUOFjweofhiuhncowuegh998wvioen8")
            print(distance)
            print("nuviwncuvewvnuiwbciuwenifuwcieuhnfo")
            if distance[-1] == "k":
                distance = float(distance[:-2])
            else:
                distance = float(distance[:-1])

            if distance <= 10.0:
                i.value = i.value + 2
            elif distance > 10.0 and distance < 15:
                i.value = i.value + 1

        else:
            print("API failed")

def do_sort(priority_list):
    result = []
    for activity in priority_list:
        result += [activity]
    result.sort(key=lambda x: x.value, reverse=True)
    return result

def is_intersects_schedule(i, j):    #i is Activity.schedule, j is user's free time
    j_start_int = int(j['start'])
    j_end_int = int(j['end'])
    i_open_int = i.open
    i_close_int = i.close

    if j['day'] == i.day:

        if j_start_int >= i_open_int and j_start_int < i_close_int:
            if j_end_int > i_close_int:
                return True

        if j_start_int < i_open_int:
            if j_end_int > i_open_int:
                if j_end_int <= i_close_int:
                    return True

    return False

def is_in_schedule(i, j):
    j_start_int = int(j['start'])
    j_end_int = int(j['end'])
    i_open_int = i.open
    i_close_int = i.close


    if j['day'] == i.day:
        if i_open_int <= j_start_int and i_close_int >= j_end_int:
            return True
        elif i_open_int >= j_start_int and i_close_int <= j_end_int:
            return True
    return False

def get_distance(origin, dest):
    api_key = "AIzaSyDe3-D4gI5E7JQGGKS3YZrUNUEHBIwrCU4"
    source = origin
    url = 'https://maps.googleapis.com/maps/api/distancematrix/json?'

    query = url + 'origins=' + source + '&destinations=' + dest + '&key=' + api_key

    req_result = requests.get(query)
    result_json = req_result.json()
    return result_json

def priority_assignment(list):
    for i in list:
        if i.value <= 2:
            i.priority = 'low'
        elif 2 < i.value and i.value < 5:
            i.priority = 'med'
        else:
            i.priority = 'hi'